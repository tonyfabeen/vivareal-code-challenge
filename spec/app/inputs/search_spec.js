'use strict';

var chai           = require('chai'),
    expect         = chai.expect,
    describedClass = require('../../../app/inputs/search');


describe('app/inputs/search', () => {
  describe('#isValid', () => {
    describe('when it is a valid input', () => {
      var input   = { "ax": 700, "ay": 700, "bx": 100, "by": 10},
          subject = new describedClass(input);

      it('the object will be valid', () => {
        expect(subject.isValid()).to.be.true;
      });

      it('will not have errors', () => {
        subject.isValid();
        console.log(subject.errors);
        expect(subject.errors).to.be.false;
      });
    });
  });
});
