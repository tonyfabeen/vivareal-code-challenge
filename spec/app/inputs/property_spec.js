'use strict'

var chai           = require('chai'),
    expect         = chai.expect,
    describedClass = require('../../../app/inputs/property');


describe('app/inputs/property', () => {
  describe('constructor', () => {
    var input   = { "x": 700, "y": 700, "beds": 4, "baths": 3, "squareMeters": 200},
        subject = new describedClass(input);

    it('sets id to object', () => {
      expect(subject._id).to.not.be.null;
      expect(subject._id).to.equal(1);
    });
  });

  describe('#isValid', () => {
    describe('a valid input', () => {
      var input   = { "x": 700, "y": 700, "beds": 4, "baths": 3, "squareMeters": 200},
          subject = new describedClass(input);

      it('the object will be valid', () => {
        expect(subject.isValid()).to.be.true;
      });

      it('will not have errors', () => {
        subject.isValid();

        expect(subject.errors).to.be.false;
      });
    });

    describe('a invalid input', () => {
      var input   = { "y": 700, "beds": 7, "baths": 5, "squareMeters": 300},
          subject = new describedClass(input);

      it('the object will not be valid', () => {
        expect(subject.isValid()).to.be.false;
      });

      it('will have errors', () => {
        subject.isValid();

        expect(subject.errors).to.not.be.null;
      });
    });

  });
});
