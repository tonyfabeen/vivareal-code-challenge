'use strict';

var chai           = require('chai'),
    expect         = chai.expect,
    Database       = require('../../../app/database/memory'),
    PropertyInput  = require('../../../app/inputs/property'),
    describedClass = require('../../../app/repositories/property');

describe('app/repositories/property', () => {
  var database = new Database();

  afterEach(() => { database.clear() });

  describe('#save', () => {
    var input    = new PropertyInput({ "x": 700, "y": 700, "beds": 4, "baths": 3, "squareMeters": 200}),
        subject  = new describedClass(input);

    beforeEach(() => { subject.save() });

    it('saves model data on database', () => {
      var expected = { "id": 1, "x": 700, "y": 700, "beds": 4, "baths": 3, "squareMeters": 200};

      expect(database.retrieve(1)).to.deep.equal(expected);
    });
  });

  describe('#findById', () => {
    describe('when it finds a property', () => {
      var input    = new PropertyInput({ "x": 700, "y": 700, "beds": 4, "baths": 3, "squareMeters": 200}),
          property = new describedClass(input);

      beforeEach(() => { property.save() });


      it('returns the property', () => {
        var expected = { "id": 1, "x": 700, "y": 700, "beds": 4, "baths": 3, "squareMeters": 200, provinces: []};
        expect(describedClass.findById(1)).to.deep.equal(expected);
      });
    });
  });

  describe('#search', () => {
    beforeEach(() => {
      var items = [{ x: 102, y: 877, squareMeters: 133 },
                   { x: 878, y: 943, squareMeters: 212 },
                   { x: 672, y: 588, squareMeters: 183 }];


      items.forEach((item) => {
        var input    = new PropertyInput(item);
        var property = new describedClass(input);

        property.save();
      });
    });

    it('returns all properties', () => {
      expect(describedClass.search([100, 500, 700, 900]).length).to.be.equal(2);
    });
  });
});
