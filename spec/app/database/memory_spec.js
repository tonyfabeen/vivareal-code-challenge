'use strict';

var chai           = require('chai'),
    expect         = chai.expect,
    sinon          = require('sinon'),
    describedClass = require('../../../app/database/memory');

describe('app/database/memory', () => {
  describe('.constructor', () => {
    var subject = new describedClass();

    it('will have a storage', () => {
      expect(subject._storage).to.not.be.undefined;
    });
  });

  describe('#persist', () => {

    describe('when the object does not have an Id', () => {
      var subject = new describedClass(),
          provinces = { "Gode" : {
                        "boundaries" : {
                          "upperLeft" : {
                            "x" : 0,
                            "y" : 1000
                          },
                          "bottomRight" : {
                            "x" : 600,
                            "y" : 500
                          }
                        }
                       }
                     };

       beforeEach(() => {
         var gode = provinces.Gode;

         subject.persist([ gode.boundaries.upperLeft.x,
                           gode.boundaries.upperLeft.y,
                           gode.boundaries.bottomRight.x,
                           gode.boundaries.bottomRight.y,
                           { province: 'Gode' }]);
       });

       afterEach(() => { subject._storage.clear(); })

      it('saves data on storage', () => {
        expect(subject._storage.all().length).to.equal(1);
      });

      it('doest not save data on index', () => {
        expect(Object.keys(subject._index).length).to.equal(0);
      });
    })

    describe('when the Object has an Id', () => {
      var subject  = new describedClass(),
          data     = [ 88, 90, 100, 500, { "id": 1,
                                           "x": 88,
                                           "y": 521,
                                           "beds": 5,
                                           "baths": 4,
                                           "squareMeters": 198 } ];

      beforeEach(() => { subject.persist(data) });

      afterEach(() => { subject._storage.clear(); })

      it('saves data on storage', () => {
        expect(subject._storage.all().length).to.equal(1);
      });

      it('saves data on index', () => {
        expect(Object.keys(subject._index).length).to.equal(1);
      });

      it('data will be the same that passed on data', () => {
        var expected = { "id": 1, "x": 88, "y": 521, "beds": 5, "baths": 4, "squareMeters": 198 };

        expect(subject.retrieve(1)).to.deep.equal(expected);
      });
    });
  });

  describe('#neighbors', () => {
    var subject = new describedClass();

    describe('when it has neighbors', () => {
      beforeEach(() => {
        var populateProvinces = () => {
          var gode = [0, 500, 600, 1000, { province: 'Gode' }];
          var ruja = [400, 500, 1100, 1000, { province: 'Ruja' }];

          subject.persist(gode);
          subject.persist(ruja);
        };

        //
        populateProvinces();
      });

      it('returns neighbors that intersects', () => {
        expect(subject.neighbors([57, 929, 57, 929])).to.deep.equal(['Gode']);
      });
    });

    describe('when it does not match neighbors', () => {
      it('returns an empty array', () => {
        expect(subject.neighbors([1, 2, 3, 4])).to.deep.equal([]);
      });
    });
  });

  describe('#search', () => {
    var subject = new describedClass();

    beforeEach(() => {
      var items = [{ x: 102, y: 877, squareMeters: 133 },
                   { x: 878, y: 943, squareMeters: 212 },
                   { x: 672, y: 588, squareMeters: 183 }];


      items.forEach((item) => {
        subject.persist([item.x,
                         item.y,
                         (item.x + (item.squareMeters/2)),
                         (item.y + (item.squareMeters/2)),
                         item]);
      });
    });

    afterEach(() => { subject.clear(); });

    describe('when it does not find items', () => {
      it('returns an empty array', () => {
        expect(subject.search([0, 100, 0, 100])).to.be.deep.equal([]);
      });
    });

    describe('when it find items', () => {
      it('returns the array of items', () => {
        //ax, by, bx, ay
        var expected = [ { x: 102, y: 877, squareMeters: 133 },
                         { x: 672, y: 588, squareMeters: 183 } ];

        expect(subject.search([100, 500, 700, 900])).to.be.deep.equal(expected);
      });
    });
  });

  describe('#nextId', () => {
    describe('when database is empty', () => {
      var subject = new describedClass();

      it('returns 1', () => {
        expect(subject.nextId()).to.be.equal(1);
      });
    });

    describe('when database is not empty', () => {
      var subject = new describedClass();

      beforeEach(() => {
        var item = { id: 1, x: 102, y: 877, squareMeters: 133 };

        subject.persist([item.x,
                         item.y,
                         (item.x + (item.squareMeters/2)),
                         (item.y + (item.squareMeters/2)),
                         item]);
      });

      it('returns next available id', () => {
        expect(subject.nextId()).to.be.equal(2);
      });
    });
  });
});
