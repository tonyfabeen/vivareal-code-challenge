'use strict';

var chai    = require('chai'),
    expect  = chai.expect,
    request = require('supertest');

describe('GET /v1/properties', () => {
  it('returns found properties', () => {
    var app = require('../../../../app');

    request(app).get('/v1/properties?ax=500&ay=800&bx=600&by=800')
                .end((err, res) => {

      if (err) return done(err);

      expect(err).to.equal.null;
      expect(res.status).to.equal(200);
      expect(res.body.foundProperties).to.equal(36);

      done();
    });
  });
});

describe('GET /v1/properties/1', () => {
  it('returns found property', () => {
    var app = require('../../../../app');

    request(app).get('/v1/properties/1')
                .end((err, res) => {

      if (err) return done(err);

      expect(err).to.equal.null;
      expect(res.status).to.equal(200);
      expect(res.body).to.deep.equal({"id":1,"x":88,"y":521,"beds":5,"baths":4,"squareMeters":198,"provinces":["Gode"]});

      done();
    });
  });
});

describe('POST /v1/properties', () => {
  it('saves the property', () => {
    var app    = require('../../../../app'),
        params = { "x": 700, "y": 250, "beds": 5, "baths": 3, "squareMeters": 200};

    request(app).post('/v1/properties')
                .send(params)
                .end((err, res) => {

      if (err) return done(err);

      expect(err).to.equal.null;
      expect(res.status).to.equal(201);

      done();
    });
  });
});
