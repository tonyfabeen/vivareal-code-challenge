var express            = require('express');
var router             = express.Router();
var PropertyInput      = require('../../inputs/property');
var SearchInput        = require('../../inputs/search');
var PropertyRepository = require('../../repositories/property');

router.get('/', function(req, res, next) {
  var input = new SearchInput(req.query);

  if (input.isValid()) {
    var properties, response;

    properties = PropertyRepository.search([req.query.ax, req.query.by, req.query.bx, req.query.ay]);

    response = { foundProperties: properties.length, properties: properties };

    res.status(200).json(response);
  } else {
    res.status(422).json({ errors: input.errors });
  }

});

router.get('/:id', function(req, res, next) {
  var property = PropertyRepository.findById(req.params.id);

  if (property) {
    res.status(200).json(property);
  } else {
    res.status(404).json({ message: 'Property does not exists!'});
  }
});

router.post('/', function(req, res, next) {
  var input = new PropertyInput(req.body);

  if (input.isValid()) {
    var repository = new PropertyRepository(input);

    repository.save();

    res.status(201).json(input.toJson());
  } else {
    res.status(422).json({ errors: input.errors });
  }
});

module.exports = router;
