'use strict';

var Database = require('../../app/database/memory');

class Property {
  //
  constructor(input) {
    this._input = input;
    this._database = new Database();
  }

  //
  save() {
    var attributes = this._input.toJson();

    this._database.persist([attributes.x,
                            attributes.y,
                            (attributes.x + (attributes.squareMeters/2)),
                            (attributes.y + (attributes.squareMeters/2)),
                            attributes]);
  }

  //
  static findById(id) {
    var database  = new Database();
    var property  = database.retrieve(id);

    if (property) {
      var neighbors = database.neighbors([property.x,
                                          property.y,
                                          property.x,
                                          property.y]);
      property.provinces = neighbors;

      return property;
    }
  }

  //
  static search(coords) {
    var database = new Database();

    return database.search(coords);
  }
}

module.exports = Property;
