'use strict';

var schema = require('js-schema');

class Search {
  constructor(attributes) {
    this._schema = schema({
      ax: Number.min(0).max(9999),
      ay: Number.min(0).max(9999),
      bx: Number.min(0).max(9999),
      by: Number.min(0).max(9999)
    });

    this._ax     = parseInt(attributes.ax),
    this._ay     = parseInt(attributes.ay),
    this._bx     = parseInt(attributes.bx),
    this._by     = parseInt(attributes.by),
    this._errors = null;
  }

  get errors() { return this._errors; }

  isValid() {
    this._errors = this._schema.errors({
      ax: this._ax,
      ay: this._ay,
      bx: this._bx,
      by: this._by
    });

    if (this._errors) {
      return false;
    } else {
      return true;
    }
  }


  //TODO: spec
  toJson() {
    return {
      id: this._id,
      x: this._x,
      y: this._y,
      beds: this._beds,
      baths: this._baths,
      squareMeters: this._squareMeters
    };
  }
}

module.exports = Search;
