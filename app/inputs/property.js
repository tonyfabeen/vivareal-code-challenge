'use strict';

var schema   = require('js-schema'),
    Database = require('../../app/database/memory'),
    database = new Database();

class Property {
  constructor(attributes) {
    this._schema = schema({
      x: Number.min(0).max(1400),
      y: Number.min(0).max(1000),
      beds: Number.min(1).max(5),
      baths: Number.min(1).max(4),
      squareMeters: Number.min(20).max(240)
    });

    if (!attributes.id) { this._id = database.nextId(); }

    this._x            = attributes.x,
    this._y            = attributes.y,
    this._beds         = attributes.beds;
    this._baths        = attributes.baths;
    this._squareMeters = attributes.squareMeters;
    this._errors       = null;
  }

  get errors() { return this._errors; }

  isValid() {
    this._errors = this._schema.errors({
      x: this._x,
      y: this._y,
      beds: this._beds,
      baths: this._baths,
      squareMeters: this._squareMeters
    });

    if (this._errors) {
      return false;
    } else {
      return true;
    }
  }


  //TODO: spec
  toJson() {
    return {
      id: this._id,
      x: this._x,
      y: this._y,
      beds: this._beds,
      baths: this._baths,
      squareMeters: this._squareMeters
    };
  }
}

module.exports = Property;
