'use strict';

var rbush    = require('rbush'),
    instance = null;

class Memory {

  //
  constructor() {
    if (!instance) { instance = this; }

    this._storage = rbush();
    this._index   = {}

    return instance;
  }

  //
  persist(data) {
    this._storage.insert(data);

    var objAttributes = data[4];

    if (objAttributes && objAttributes.hasOwnProperty('id')) {
      this._index[objAttributes.id] = objAttributes;
    }
  }

  //
  all() {
    return this._storage.all();
  }

  //
  retrieve(id) {
    return this._index[id];
  }

  //
  neighbors(coords) {
    var neighbors = this._storage.search(coords);

    if (neighbors.length > 0) {
      var justProvinces = (item) => {
        return item[4].hasOwnProperty('province');
      };

      return neighbors.filter(justProvinces)
                      .map((item) => { return item[4].province });
    } else {
      return [];
    }
  }

  //
  search(coords) {
    var items = this._storage.search(coords);

    if (items.length > 0) {
      var discardProvinces = (item) => {
        return !item[4].hasOwnProperty('province');
      };

      return items.filter(discardProvinces)
                  .map((item) => { return item[4] });
    } else {
      return [];
    }
  }

  //
  nextId() {
    var ids = Object.keys(this._index);

    if (ids === undefined || ids.length == 0) {
      return 1;
    }

    return Math.max(...ids) + 1;
  }

  //
  clear() {
    this._storage.clear();
    this._index = {};
  }
}

module.exports = Memory;
