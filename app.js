var express = require('express');
var path = require('path');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

//
var Database = require('./app/database/memory');
var database = new Database();
var contentFromFile = (file) => {
  var fs = require('fs');

  if (fs.existsSync(file)) {
    var data = fs.readFileSync(file);

    return JSON.parse(data);
  }
};

console.log('Loading Database ...');

var populateProvinces = () => {
  var gode = [0, 500, 600, 1000, { province: 'Gode' }];
  var ruja = [400, 500, 1100, 1000, { province: 'Ruja' }];
  var jaby = [1100, 500, 1400, 1000, { province: 'Jaby'}];
  var scavy = [0, 0, 600, 500, { province: 'Scavy'}];
  var groola = [600, 0, 800, 500, { province: 'Groola'}];
  var nova = [800, 0, 1400, 500, { province: 'Nova'}];

  database.persist(gode);
  database.persist(ruja);
  database.persist(jaby);
  database.persist(scavy);
  database.persist(groola);
  database.persist(nova);
};

//
populateProvinces();
var content = contentFromFile('./properties.json');

content.properties.forEach((item) => {
  // [minX, minY, maxX, maxY]
  // [upper-left-x, bottom-right-y, bottom-right-x, upper-left-y]
  var x = database.persist([item.x, item.y, (item.x + (item.squareMeters/2)), (item.y + (item.squareMeters/2)), item]);
});

console.log('Database Loaded !');

//
var routes = require('./app/routes/index');
var propertiesV1Routes = require('./app/routes/v1/properties');

//
var app = express();
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

app.use('/', routes);
app.use('/v1/properties', propertiesV1Routes);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500).json({
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500)
     .json('error', {
       message: err.message,
       error: {}
  });
});


module.exports = app;
