# Viva Real Challenge

Esse projeto é parte do processo de avaliação da Viva Real.

### Dependencias
  - NodeJS: 5.10.0
  - NPM: 3.8.3

  Antes de rodar o projeto é necessário atualizar suas dependencias internas executando :
```sh
$ npm install
```

## Executando o Projeto localmente
```sh
$ npm start
```

## Executando a suíte de testes
```sh
$ npm test
```

## Deployment
  A maneira mais fácil de executar o deployment do projeto é utilizar a plataforma Heroku.
  Basta seguir as orientações na página `https://devcenter.heroku.com/articles/getting-started-with-nodejs#deploy-the-app`

  Um ambiente de testes está disponível no endereço : `https://shielded-tundra-89807.herokuapp.com`

## Autor
Tony Fabeen Oreste <<tony.fabeen@gmail.com>>

